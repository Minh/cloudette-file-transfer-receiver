package com.cloudette.security;

/**
 * Simple AES encryption
 * @author Minh-Long
 *
 */

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.io.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.*;

public class AES {
	
	private SecretKeySpec secretKey;
	private byte[] key;
	private int keySize;
	
	/**
	 * Create instance to decrypt using already generated secret key
	 * @param secretKey SecretKey used to decrypt
	 */
	public AES(SecretKeySpec secretKey) {
		this.secretKey = secretKey;
	}
	
	/**
	 * Create new instance for AES
	 * @param key Key used to create secretKey
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 */
	public AES(int keySize) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if (keySize == 16 || keySize == 24 || keySize == 32) {
			
			this.keySize = keySize;
		} else {
			System.out.println("Switch to default key size 16 bytes");
			this.keySize = 16;
		}
	}
	
	/**
	 * generate keys
	 * @param key
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public void genKey(String key) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		MessageDigest sha = null;
		this.key = key.getBytes("UTF-8");
		sha = MessageDigest.getInstance("SHA-1");
		this.key = sha.digest(this.key);
		this.key = Arrays.copyOf(this.key, keySize); // uses the highest key size
		this.secretKey = new SecretKeySpec(this.key, "AES");
	}
	
	/**
	 * SecretKey to char[]
	 * @return
	 */
	public char[] prepKeyToEncrypt() {
		char[] hex = org.apache.commons.codec.binary.Hex.encodeHex(this.secretKey.getEncoded());
		return hex;
	}
	
	/**
	 * SecretKey to char[] 
	 * @param secretKey SecretKey to be converted
	 * @return
	 */
	public char[] prepKeyToEncrypt(SecretKey secretKey) {
		char[] hex = org.apache.commons.codec.binary.Hex.encodeHex(secretKey.getEncoded());
		return hex;
	}
	
	/**
	 * Save SecretKey to file
	 * @param file File location
	 * @throws IOException
	 */
	public void saveKey(File file) throws IOException {
		char[] hex = prepKeyToEncrypt();
		FileUtils.writeStringToFile(file, String.valueOf(hex));
	}
	
	/**
	 * Save SecretKey to file
	 * @param file File location
	 * @param secretKey SecretKey to be saved
	 * @throws IOException
	 */
	public void saveKey(File file, SecretKey secretKey) throws IOException {
		char[] hex = prepKeyToEncrypt(secretKey);
		FileUtils.writeStringToFile(file, String.valueOf(hex));
	}
	
	/**
	 * Load String key into SecretKey format
	 * @param secretKey String to be loaded
	 * @throws DecoderException 
	 */
	public void loadKey(String key) throws DecoderException {
		byte[] encoded = org.apache.commons.codec.binary.Hex.decodeHex(key.toCharArray());
		
		this.secretKey = new SecretKeySpec(encoded, "AES");
	}
	
	/**
	 * Load char[] into SecretKey format
	 * @param key Char[] to be loaded
	 * @throws DecoderException
	 */
	public void loadKey(char[] key) throws DecoderException {
		byte[] encoded = org.apache.commons.codec.binary.Hex.decodeHex(key);
		this.secretKey = new SecretKeySpec(encoded, "AES");
	}
	
	/**
	 * Load Byte[] into SecretKey format
	 * @param encoded Byte[] to be loaded
	 */
	public void loadKey(byte[] encoded) {
		this.secretKey = new SecretKeySpec(key, "AES");
	}
	
	/**
	 * Load secretKey from file
	 * @param file Location of file
	 * @throws IOException
	 * @throws DecoderException
	 */
	public void loadKey(File file) throws IOException, DecoderException {
		String data = new String(FileUtils.readFileToByteArray(file));
		byte[] encoded = org.apache.commons.codec.binary.Hex.decodeHex(data.toCharArray());
		
		this.secretKey = new SecretKeySpec(encoded, "AES");
	}
	
	/**
	 * Encrypt message
	 * @param message Message to encrypt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public String encrypt(String message) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
		return (new String(Base64.encodeBase64String(cipher.doFinal(message.getBytes("UTF-8")))));
	}
	
	/**
	 * Decrypt message
	 * @param message Message to decrypt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String decrypt(String message) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		
		return (new String(cipher.doFinal(Base64.decodeBase64(message))));
	}
	
	/**
	 * Get secret key
	 * @return
	 */
	public SecretKeySpec getSecretKey() {
		return this.secretKey;
	}
}
