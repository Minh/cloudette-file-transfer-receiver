package com.cloudette.security;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Simple RSA public key encryption and decryption implementation
 * @author Minh-Long
 *
 */

public class RSA {

	private BigInteger n;
	private BigInteger privateKey;
	private BigInteger publicKey;
	
	RSAPublicKey pub;
	RSAPrivateKey privKey;
	
	private int bitlen = 1024;
	
	/**
	 * Create instance that encrypt using a public key
	 */
	public RSA(BigInteger n, BigInteger publicKey) {
		this.n = n;
		this.publicKey = publicKey;
	}
	
	/**
	 * Get public key
	 * @return
	 */
	public BigInteger getPubKey() {
		return this.pub.getPublicExponent();
	}
	
	/**
	 * Get private key
	 * @return
	 */
	public BigInteger getPrivKey() {
		return this.privKey.getPrivateExponent();
	}
	
	/**
	 * Generate private and public key under KeyFactory 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public void genPrivatePublicKey () throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeyFactory factory_rsa = KeyFactory.getInstance("RSA");
		RSAPublicKeySpec spec = new RSAPublicKeySpec(n, this.publicKey);
		this.pub = (RSAPublicKey) factory_rsa.generatePublic(spec);
		
		RSAPrivateKeySpec privKeySpec = new RSAPrivateKeySpec(this.n, this.privateKey);
		this.privKey = (RSAPrivateKey) factory_rsa.generatePrivate(privKeySpec);
	}
	
	/**
	 * Decrypt AES secret key from RSA
	 * @param data Data to decrypt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public SecretKey decryptAESKey(byte[] data) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		SecretKey key = null;

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, this.privKey);
		
		key = new SecretKeySpec(cipher.doFinal(data), "AES");
		
		return key;
	}
	
	/**
	 * Encrypt AES secret key
	 * @param secretKey Secret key to encrypt
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws InvalidKeySpecException
	 */
	public byte[] encryptAESKey(SecretKeySpec secretKey) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
		KeyFactory factory_rsa = KeyFactory.getInstance("RSA");
		RSAPublicKeySpec spec = new RSAPublicKeySpec(n, this.publicKey);
		this.pub = (RSAPublicKey) factory_rsa.generatePublic(spec);
		
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, pub);
		byte[] key = cipher.doFinal(secretKey.getEncoded());
		
		return key;
	}
	
	/**
	 * Create public and private keys
	 * @param bits Specify bit length
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	public RSA(int bits) throws NoSuchAlgorithmException, InvalidKeySpecException {
		bitlen = bits;
		generateKeys();
		genPrivatePublicKey();
	}
	
	/**
	 * Encrypt plaintext
	 * @param message Plaintext to encrypt
	 * @return
	 */
	public synchronized String encrypt(String message) {
		return (new BigInteger(message.getBytes()).modPow(this.publicKey, n)).toString();
	}
	
	/**
	 * Encrypt BigInteger
	 * @param message BigInteger to encrypt
	 * @return
	 */
	public synchronized BigInteger encrypt(BigInteger message) {
		return message.modPow(this.publicKey, n);
	}
	
	/**
	 * Decrypt message
	 * @param message Encrypted message to decrypt
	 * @return
	 */
	public synchronized String decrypt(String message) {
		return new String(new BigInteger(message).modPow(privateKey, n).toByteArray());
	}
	
	/**
	 * Decrypt BigInteger
	 * @param message Message to decrypt
	 * @return
	 */
	public synchronized BigInteger decrypt(BigInteger message) {
		return message.modPow(this.privateKey, n);
	}
	
	/**
	 * Generate new RSA keys
	 */
	public synchronized void generateKeys() {
		SecureRandom r = new SecureRandom();
		BigInteger p = new BigInteger(bitlen/2, 100, r);
		BigInteger q = new BigInteger(bitlen/2, 100, r);
		
		this.n = p.multiply(q);
		BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		
		publicKey = new BigInteger("3");
		
		while(m.gcd(publicKey).intValue() > 1) {
			this.publicKey = this.publicKey.add(new BigInteger("2"));
		}
		this.privateKey = publicKey.modInverse(m);
	}
	
	/**
	 * Get RSA public key
	 * @return
	 */
	public BigInteger getPublicKey() {
		return this.publicKey;
	}
	
	/**
	 * Get RSA private key
	 * @return
	 */
	public BigInteger getPrivateKey() {
		return this.privateKey;
	}
	
	/**
	 * Get the modulus
	 * @return
	 */
	public BigInteger getModulus() {
		return this.n;
	}
}
