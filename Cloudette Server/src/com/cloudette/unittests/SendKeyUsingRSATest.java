package com.cloudette.unittests;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cloudette.security.AES;
import com.cloudette.security.RSA;

public class SendKeyUsingRSATest {

	private final int key128 = 16;
	private final int key192 = 24;
	private final int key256 = 32;
	
	private final int BIT_SIZE1024 = 1024;
	private final int BIT_SIZE2048 = 2048;
	
	// generate random password
	private final String password = UUID.randomUUID().toString().replaceAll("-", "");
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testSendAES256EncryptStringRSA2048() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key256);
		AES aes1 = new AES(this.key256);
		RSA rsa = new RSA(BIT_SIZE2048);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}
	
	@Test
	public void testSendAES192EncryptStringRSA2048() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key192);
		AES aes1 = new AES(this.key192);
		RSA rsa = new RSA(BIT_SIZE2048);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}
	
	@Test
	public void testSendAES128EncryptStringRSA2048() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key128);
		AES aes1 = new AES(this.key128);
		RSA rsa = new RSA(BIT_SIZE2048);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}
	
	@Test
	public void testSendAES256EncryptStringRSA1024() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key256);
		AES aes1 = new AES(this.key256);
		RSA rsa = new RSA(BIT_SIZE1024);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}
	
	@Test
	public void testSendAES192EncryptStringRSA1024() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key192);
		AES aes1 = new AES(this.key192);
		RSA rsa = new RSA(BIT_SIZE1024);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}
	
	@Test
	public void testSendAES128EncryptStringRSA1024() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DecoderException {
		AES aes = new AES(this.key128);
		AES aes1 = new AES(this.key128);
		RSA rsa = new RSA(BIT_SIZE1024);
		
		// generate secret key
		aes1.genKey(UUID.randomUUID().toString().replaceAll("-", ""));
		aes.genKey(this.password);
		
		// encode to hex
		char[] hex = aes.prepKeyToEncrypt();
		
		// encrypt
		String encrypted = rsa.encrypt(String.valueOf(hex));
		// decrypt
		String decrypted = rsa.decrypt(encrypted);
		
		// load secret key
		aes1.loadKey(decrypted);
		
		assertEquals(aes.getSecretKey(), aes1.getSecretKey());
	}

}
