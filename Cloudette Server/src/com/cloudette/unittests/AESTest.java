package com.cloudette.unittests;

import static org.junit.Assert.*;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.DecoderException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cloudette.security.AES;

public class AESTest {

	private final int key128 = 16;
	private final int key192 = 24;
	private final int key256 = 32;
	
	// generate random message
	private final String message = UUID.randomUUID().toString();
	private final byte[] byteMessage = message.getBytes();
	private final BigInteger bigIntegerMessage = new BigInteger(message.getBytes());
	// generate random password
	private final String password = UUID.randomUUID().toString().replaceAll("-", "");
	//generate random filename
	private final String filename = UUID.randomUUID().toString().replaceAll("-", "");
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testSaveLoadSecretKeyAES256() throws UnsupportedEncodingException, NoSuchAlgorithmException, DecoderException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		
		SecretKey secret = aes.getSecretKey();
		
		char[] encoded = aes.prepKeyToEncrypt();
		aes.loadKey(encoded);
		
		assertEquals(secret, aes.getSecretKey());
	}
	
	@Test
	public void testSaveLoadSecretKeyAES192() throws UnsupportedEncodingException, NoSuchAlgorithmException, DecoderException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		
		SecretKey secret = aes.getSecretKey();
		
		char[] encoded = aes.prepKeyToEncrypt();
		aes.loadKey(encoded);
		
		assertEquals(secret, aes.getSecretKey());
	}
	
	@Test
	public void testSaveLoadSecretKeyAES128() throws UnsupportedEncodingException, NoSuchAlgorithmException, DecoderException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		
		SecretKey secret = aes.getSecretKey();
		
		char[] encoded = aes.prepKeyToEncrypt();
		aes.loadKey(encoded);
		
		assertEquals(secret, aes.getSecretKey());
	}
	
	@Test
	public void testSaveLoadFileSecretKeyAES256() throws NoSuchAlgorithmException, IOException, DecoderException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		SecretKey secretKey = aes.getSecretKey();
		// create file
		File file = new File(this.filename);
		aes.saveKey(file, aes.getSecretKey());
		//overwrite previous secretKey
		aes.loadKey(file);
		
		file.delete();
		
		if(file.exists()) {
			fail("Failed to delete file.");
		}
		
		assertEquals(secretKey, aes.getSecretKey());
	}
	
	@Test
	public void testSaveLoadFileSecretKeyAES192() throws NoSuchAlgorithmException, IOException, DecoderException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		SecretKey secretKey = aes.getSecretKey();
		// create file
		File file = new File(this.filename);
		aes.saveKey(file, aes.getSecretKey());
		//overwrite previous secretKey
		aes.loadKey(file);
		
		file.delete();
		
		if(file.exists()) {
			fail("Failed to delete file.");
		}
		
		assertEquals(secretKey, aes.getSecretKey());
	}
	
	@Test
	public void testSaveLoadFileSecretKeyAES128() throws NoSuchAlgorithmException, IOException, DecoderException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		SecretKey secretKey = aes.getSecretKey();
		// create file
		File file = new File(this.filename);
		aes.saveKey(file, aes.getSecretKey());
		//overwrite previous secretKey
		aes.loadKey(file);
		
		file.delete();
		
		if(file.exists()) {
			fail("Failed to delete file.");
		}
		
		assertEquals(secretKey, aes.getSecretKey());
	}
	
	@Test
	public void testStringAES256AlreadyGeneratedKey() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		AES aes256 = new AES(this.key256);
		aes256.genKey(this.password);
		String encrypted = aes256.encrypt(this.message);
		
		AES aes = new AES(aes256.getSecretKey());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test
	public void testStringAES192AlreadyGeneratedKey() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		AES aes192 = new AES(this.key192);
		aes192.genKey(this.password);
		String encrypted = aes192.encrypt(this.message);
		
		AES aes = new AES(aes192.getSecretKey());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test
	public void testStringAES128AlreadyGeneratedKey() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		AES aes128 = new AES(this.key128);
		aes128.genKey(this.password);
		String encrypted = aes128.encrypt(this.message);
		
		AES aes = new AES(aes128.getSecretKey());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test
	public void testStringAES256() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message,decrypted);
	}
	
	@Test
	public void testByteAES256() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.byteMessage.toString(), decrypted);
	}
	
	@Test
	public void testBigIntegerAES256() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.bigIntegerMessage.toString(), decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testStringDecryptDifferentAES256ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}

	@Test(expected = BadPaddingException.class)
	public void testByteDecryptDifferentAES256ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testBigIntegerDecryptDifferentAES256ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key256);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test
	public void testStringAES192() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message,decrypted);
	}
	
	@Test
	public void testByteAES192() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.byteMessage.toString(), decrypted);
	}
	
	@Test
	public void testBigIntegerAES192() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.bigIntegerMessage.toString(), decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testStringDecryptDifferentAES192ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}

	@Test(expected = BadPaddingException.class)
	public void testByteDecryptDifferentAES192ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testBigIntegerDecryptDifferentAES192ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key192);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test
	public void testStringAES128() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message,decrypted);
	}
	
	@Test
	public void testByteAES128() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.byteMessage.toString(), decrypted);
	}
	
	@Test
	public void testBigIntegerAES128() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.bigIntegerMessage.toString(), decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testStringDecryptDifferentAES128ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.message);
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}

	@Test(expected = BadPaddingException.class)
	public void testByteDecryptDifferentAES128ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.byteMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test(expected = BadPaddingException.class)
	public void testBigIntegerDecryptDifferentAES128ExpectFail() throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		AES aes = new AES(this.key128);
		aes.genKey(this.password);
		String encrypted = aes.encrypt(this.bigIntegerMessage.toString());
		
		aes.genKey(UUID.randomUUID().toString());
		
		String decrypted = aes.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
}
