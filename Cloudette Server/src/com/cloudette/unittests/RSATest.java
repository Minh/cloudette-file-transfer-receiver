package com.cloudette.unittests;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.cloudette.security.RSA;

@RunWith(JUnit4.class)
public class RSATest {

	private final String message = "Yellow and Black Border Collies";
	private final byte[] byteMessage = message.getBytes();
	private final BigInteger bigIntegerMessage = new BigInteger(message.getBytes());
	private final int BIT_SIZE1024 = 1024;
	private final int BIT_SIZE2048 = 2048;
	
	@BeforeClass
	public static void testSetUp() {
	}

	@AfterClass
	public static void testCleanup() {
	}
	
	public void testPublicKey2048BigInteger() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		rsa.generateKeys();
		rsa.genPrivatePublicKey();
		
		assertEquals(rsa.getPublicKey(), rsa.getPubKey());
	}

	public void testPublicKey1024BigInteger() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		rsa.generateKeys();
		rsa.genPrivatePublicKey();
		
		assertEquals(rsa.getPublicKey(), rsa.getPubKey());
	}
	
	@Test
	public void testPrivateKey2048BigInteger() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		rsa.generateKeys();
		rsa.genPrivatePublicKey();
		
		System.out.println("First private key: " + rsa.getPrivateKey());
		System.out.println("Second private key: " + rsa.getPrivKey());
		
		assertEquals(rsa.getPrivateKey(), rsa.getPrivKey());
	}
	
	@Test
	public void testPrivateKey1024BigInteger() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		rsa.generateKeys();
		rsa.genPrivatePublicKey();
		
		System.out.println("First private key: " + rsa.getPrivateKey().toString());
		System.out.println("Second private key: " + rsa.getPrivKey().toString());
		
		assertEquals(rsa.getPrivateKey(), rsa.getPrivKey());
	}
	
	@Test
	public void testStringRSA2048() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		String encrypted = rsa.encrypt(this.message);
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(message, decrypted);
	}
	
	@Test
	public void testByteRSA2048() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		String encrypted = rsa.encrypt(this.byteMessage.toString());
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(byteMessage.toString(), decrypted);
	}
	
	@Test
	public void testBigIntegerRSA2048() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		BigInteger encrypted = rsa.encrypt(this.bigIntegerMessage);
		BigInteger decrypted = rsa.decrypt(encrypted);
		
		assertEquals(bigIntegerMessage,decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testStringDecryptDifferentRSA2048ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		String encrypted = rsa.encrypt(this.message);
		
		rsa.generateKeys();
		
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testByteDecryptDifferentRSA2048ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		String encrypted = rsa.encrypt(this.byteMessage.toString());
		
		rsa.generateKeys();
		
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.byteMessage, decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testBigIntegerDecryptDifferentRSA2048ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE2048);
		BigInteger encrypted = rsa.encrypt(this.bigIntegerMessage);
		
		rsa.generateKeys();
		
		BigInteger decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.bigIntegerMessage, decrypted);
	}

	
	@Test
	public void testStringRSA1024() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		String encrypted = rsa.encrypt(this.message);
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(message, decrypted);
	}
	
	@Test
	public void testByteRSA1024() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		String encrypted = rsa.encrypt(this.byteMessage.toString());
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(byteMessage.toString(), decrypted);
	}
	
	@Test
	public void testBigIntegerRSA1024() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		BigInteger encrypted = rsa.encrypt(this.bigIntegerMessage);
		BigInteger decrypted = rsa.decrypt(encrypted);
		
		assertEquals(bigIntegerMessage,decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testStringDecryptDifferentRSA1024ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		String encrypted = rsa.encrypt(this.message);
		
		rsa.generateKeys();
		
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.message, decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testByteDecryptDifferentRSA1024ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		String encrypted = rsa.encrypt(this.byteMessage.toString());
		
		rsa.generateKeys();
		
		String decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.byteMessage, decrypted);
	}
	
	@Test(expected = AssertionError.class)
	public void testBigIntegerDecryptDifferentRSA1024ExpectFail() throws NoSuchAlgorithmException, InvalidKeySpecException {
		RSA rsa = new RSA(this.BIT_SIZE1024);
		BigInteger encrypted = rsa.encrypt(this.bigIntegerMessage);
		
		rsa.generateKeys();
		
		BigInteger decrypted = rsa.decrypt(encrypted);
		
		assertEquals(this.bigIntegerMessage, decrypted);
	}

}
