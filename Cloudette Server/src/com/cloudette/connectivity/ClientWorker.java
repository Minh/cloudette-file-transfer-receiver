package com.cloudette.connectivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.io.IOUtils;

public class ClientWorker extends Thread {

	private Socket incoming = null;
	// need to change path
	private final String PATH = "user.home";
	
	/**
	 * Class ClientWorker constructor
	 * @param incoming Assign socket for server
	 */
	public ClientWorker(Socket incoming) {
		this.incoming = incoming;
	}
	
	/**
	 * Save file received from socket
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private void saveFile() throws IOException {
		OutputStream out = incoming.getOutputStream();
		InputStream in = new FileInputStream(PATH);
		IOUtils.copy(in, out);
		
		incoming.close();
	}
	
	/**
	 * Exception message
	 * @param msg Exception message
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private void throwException(String msg) throws Exception {
		throw new Exception(msg);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String request = null;
		BufferedReader in = null;
		
		try {
			in = new BufferedReader(new InputStreamReader(incoming.getInputStream()));
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		try {
			request = in.readLine();
			System.out.println("Request: " + request);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(incoming.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.println("The command is not readable");
		
		try {
			in.close();
			this.incoming.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
