package com.cloudette.connectivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
	
	private Socket client = null;
	private String server;
	private int port = 0;
	private String answer;
	
	public Client(String server, int port) {
		this.server = server;
		this.port = port;
	}

	/**
	 * Send request to server
	 * @param request The specified for server
	 */
	public void sendToSever(String request) {
		printMsg("Start sending request.");
		try {
			client = new Socket(this.server, this.port);
			PrintWriter out = new PrintWriter(client.getOutputStream(), true);
			out.print(request);
			printMsg("Request has been send.");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			
			this.answer = in.readLine();
			
			in.close();
			out.close();
			client.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			printMsg("Make sure the server is running and try again.");
		}
	}
	
	/**
	 * Get answer of server
	 * @return
	 */
	public String getAnswer() {
		return this.answer;
	}
	
	private void printMsg(String msg) {
		System.out.println(msg);
	}
}
