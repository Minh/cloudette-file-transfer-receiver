package com.cloudette.connectivity;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class Server {
	
	private int port = 0;
	private String hostname;
	private String hostIP;
	private ServerSocket server = null;
	
	/**
	 * Class Server constructor
	 */
	public Server() {
	}
	
	/**
	 * Set server port
	 * @param port Specify server port
	 */
	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * Set hostname of the server
	 * @throws UnknownHostException
	 */
	private void setHost() throws UnknownHostException {
		this.hostname = InetAddress.getLocalHost().getHostName();
	}
	
	/**
	 * Get hostname
	 * @return
	 */
	private String getHost() {
		return this.hostname;
	}
	
	/**
	 * Set host ip address
	 * @throws UnknownHostException
	 */
	private void setIP() throws UnknownHostException {
		this.hostIP = InetAddress.getLocalHost().getHostAddress();
	}
	
	/**
	 * Get ip
	 * @return
	 */
	private String getIP() {
		return this.hostIP;
	}
	
	/**
	 * Set up Server
	 * @param port Specify server port
	 * @throws IOException 
	 */
	public void setUpServer(int port) throws IOException {
		setPort(port);
		setHost();
		setIP();
		
		printMsg("Server started on " + getHost() + " with IP: " + getIP());
		this.server = new ServerSocket(this.port);
	}
	
	/**
	 * Listens to port
	 */
	public void listener() {
		while(true) {
			ClientWorker clientWorker;
			
			try {
				clientWorker = new ClientWorker(server.accept());
				Thread t = new Thread(clientWorker);
				t.start();
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}
	
	/**
	 * Print message
	 * @param msg
	 */
	private void printMsg(String msg) {
		System.out.println(msg);
	}
}
