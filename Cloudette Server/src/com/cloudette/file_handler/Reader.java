package com.cloudette.file_handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reader {

	private BufferedReader br = null;
	private String content;
	private String path;
	private boolean debug = false;
	
	/**
	 * Class Reader constructor
	 * @param path Specify path of file to be read
	 */
	public Reader(String path) {
		this.path = path;
	}
	
	/**
	 * Read TXT file
	 * @throws FileNotFoundException, IOException 
	 */
	private void readTxt() throws FileNotFoundException, IOException {
		String sCurrentLine;
		br = new BufferedReader(new FileReader(path));
		
		while((sCurrentLine = br.readLine()) != null) {
			content += sCurrentLine;
		}
	}
	/**
	 * Read file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void read() throws FileNotFoundException, IOException {
		this.content = "";
		
		if (this.debug == true) {
			if (exist() == true) {
				printMsg("File exists");
			} else {
				printMsg("File does not exist.");
			}

			if (canRead() == true) {
				printMsg("Can read file.");
			} else {
				printMsg("Can not read file.");
			}

			if (isPath() == true) {
				printMsg("Is a directory.");
			} else {
				printMsg("Is not a directory.");
			}

			if (isRegularFile() == true) {
				printMsg("Is a file.");
			} else {
				printMsg("Is not a file.");
			}
		}
		
		if (this.debug == true)
			printMsg("Start reading");
		
		readTxt();
		
		if (this.debug == true) {
			printMsg("End reading");
			printMsg("File content:" + "\n" + this.content);
		}
		
		clean();
	}
	
	private boolean isRegularFile() {
		File f = new File(this.path);
		return f.isFile();
	}
	
	private boolean isPath() {
		File f = new File(this.path);
		return f.isDirectory();
	}
	
	private boolean canRead() {
		File f = new File(this.path);
		return f.canRead();
	}
	
	private boolean exist() {
		File f = new File(this.path);
		return f.exists();
	}
	
	/**
	 * Clean up
	 * @throws IOException
	 */
	private void clean() throws IOException {
		if (br != null) {
			br.close();
		}
	}
	
	/**
	 * Get file content;
	 * @return
	 */
	public String getContent() {
		return this.content;
	}
	
	private void printMsg(String msg) {
		System.out.println(msg);
	}
}
