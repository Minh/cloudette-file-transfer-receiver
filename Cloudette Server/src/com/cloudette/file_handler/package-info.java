/**
 * The Reader reads file content. At the moment it can only read reliably text files.
 */
/**
 * @author Minh-Long
 *
 */
package com.cloudette.file_handler;