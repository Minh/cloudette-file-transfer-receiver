package com.cloudette.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.cloudette.file_handler.Reader;

public class Data {

	private String path;
	private String ext;
	private String content;
	private byte[] byteContent;
	
	
	public Data(String path) {
		this.path = path;
		setExt();
	}
	
	/**
	 * Create Checksum
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	private byte[] createChecksum() throws NoSuchAlgorithmException, IOException {
		InputStream fis = new FileInputStream(this.path);
		byte[] buffer = new byte[1024];
		MessageDigest complete = MessageDigest.getInstance("SHA-256");
		int numRead;
		do {
			numRead = fis.read(buffer);
			if (numRead > 0) {
				complete.update(buffer, 0, numRead);
			}
		} while (numRead != -1);
		fis.close();
		
		return complete.digest();
	}
	
	/**
	 * Get Checksum
	 * @return
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 */
	public String getChecksum() throws NoSuchAlgorithmException, IOException {
		byte[] checkSum = createChecksum();
		String result = "";
		for(int i=0;i<checkSum.length;i++) {
			result += Integer.toString((checkSum[i] & 0xff) + 0x100, 16).substring(1); 
		}
		return result;
	}
	
	/**
	 * Shows information of the file
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 */
	public void summary() throws NoSuchAlgorithmException, IOException {
		System.out.println("\nData summary:\n");
		System.out.println("File path: " + this.path);
		System.out.println("File extension: " + this.ext);
		System.out.println("Checksum: " + getChecksum());
		System.out.println("File content:");
		System.out.println(this.content);
	}

	/**
	 * Read content from file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void setContent() throws FileNotFoundException, IOException {
		Reader reader = new Reader(this.path);
		reader.read();
		
		this.content = reader.getContent();
		setByte();
	}
	
	/**
	 * Set extension of file
	 */
	private void setExt() {
		int i = path.lastIndexOf('.');
		if (i > 0) {
			ext = path.substring(i+1);
		}
		else {
			ext = null;
		}
	}
	
	/**
	 * Get extension of file
	 * @return
	 */
	public String getExt() {
		return this.ext;
	}
	
	/**
	 * Get content of file
	 * @return
	 */
	public String getContent() {
		return this.content;
	}
	
	/**
	 * Set content in byte format
	 */
	private void setByte() {
		byteContent = this.content.getBytes();
	}
	
	/**
	 * Get content in byte format
	 * @return
	 */
	public byte[] getByte() {
		return this.byteContent;
	}
}
