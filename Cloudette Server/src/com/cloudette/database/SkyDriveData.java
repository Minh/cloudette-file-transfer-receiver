package com.cloudette.database;

public class SkyDriveData {

	private int id;
	private String folderPath;
	private String filePath;
	private String folderId;
	private String fileId;
	
	public SkyDriveData() {
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}
	
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getFolderId() {
		return this.folderId;
	}
	
	public String getFolderPath() {
		return this.folderPath;
	}
	
	public String getFileId() {
		return this.fileId;
	}
	
	public String getFilePath() {
		return this.filePath;
	}
}
