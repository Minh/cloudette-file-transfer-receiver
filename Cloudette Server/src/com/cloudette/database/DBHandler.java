package com.cloudette.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBHandler {
	
	private Connection c = null;
	private String database;
	
	public DBHandler(String database) {
		this.database = database;
	}
	
	// set new database name
	public void setDbName(String database) {
		this.database = database;
	}
	
	// connect to database
	public void connect() throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		this.c = DriverManager.getConnection("jdbc:sqlite:"+this.database);
		System.out.println("Opened database successfully");
	}
	
	// close database connection
	public void close() throws SQLException {
		if(this.c != null) {
			this.c.close();
			System.out.println("Closed database successfully");
		}
	}
	
	// create database
	public void createDatabase() throws SQLException, ClassNotFoundException {
		connect();
		
		Statement stmt = this.c.createStatement();
		String sql = "CREATE DATABASE " + this.database;
		stmt.executeUpdate(sql);
		
		close();
	}
	
	public void createDatabase(String database) throws SQLException, ClassNotFoundException {
		connect();
		
		Statement stmt = this.c.createStatement();
		String sql = "CREATE DATABASE " + database;
		stmt.executeUpdate(sql);
		
		close();
	}
	
	// create table
	public void createTable(String table) throws SQLException, ClassNotFoundException {
		connect();
		Statement stmt = this.c.createStatement();
		String sql = "CREATE TABLE "+ table + " " +
					 "(ID INT PRIMARY 	KEY			NOT NULL," +
					 "FOLDER_NAME		TEXT		NOT NULL," +
					 "FOLDER_ID			TEXT		NOT NULL," +
					 "FILE_NAME			TEXT		NOT NULL," +
					 "FILE_ID			TEXT		NOT NULL)";
		stmt.executeUpdate(sql);
		stmt.close();
	}
	
	/**
	 * insert data into table
	 * @param table
	 * @param id
	 * @param folder_name
	 * @param folder_id
	 * @param filename
	 * @param file_id
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void insert(String table, int id, String folder_name, String folder_id, String filename, String file_id) throws ClassNotFoundException, SQLException {
		connect();
		
		Statement stmt = this.c.createStatement();
		String sql = "INSERT INTO " + table + " VALUES(" + id + ","+ "\'" + folder_name + "\'" + "," + "\'" + folder_id + "\'" + "," + "\'" + filename + "\'" + "," + "\'" + file_id + "\'" +")";
		stmt.executeUpdate(sql);
		stmt.close();
		
		System.out.println("Records created successfully.");
	}
	
	/**
	 * update table
	 * @param table Selected table to be updated
	 * @param folder_name Folder name to be updated
	 * @param folder_id Folder id to be updated
	 * @param filename File name to be updated
	 * @param file_id File id to be updated
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void update(String table, String folder_name, String folder_id, String filename, String file_id) throws SQLException, ClassNotFoundException {
		Statement stmt = this.c.createStatement();
		
		String sql = "UPDATE " + table + " set FOLDER_NAME = " + folder_name + " where FOLDER_ID = " + folder_id + ");";
		stmt.executeUpdate(sql);
	}
	
	// get all data from sqlite
	public List<SkyDriveData> getTable(String table) throws ClassNotFoundException, SQLException {
		connect();
		Statement stmt = this.c.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM " + table);
		ArrayList<SkyDriveData> skyDriveData = new ArrayList<SkyDriveData>();
		while(rs.next()) {
			SkyDriveData skyData = new SkyDriveData();
			skyData.setId(rs.getInt("id"));
			skyData.setFolderId(rs.getString("folder_id"));
			skyData.setFolderId(rs.getString("folder_name"));
			skyData.setFileId(rs.getString("file_id"));
			skyData.setFilePath(rs.getString("file_name"));
			skyDriveData.add(skyData);
		}
		rs.close();
		close();
		stmt.close();
		return skyDriveData;
	}
	
	// print out query result
	private void printQuery(String table, String name, String nameId) throws ClassNotFoundException, SQLException {
		ArrayList<SkyDriveData> rs = (ArrayList<SkyDriveData>) getTable(table);
		
		for(int i=0;i<rs.size();i++) {
			SkyDriveData data = rs.get(i);
			int id = data.getId();
			String folder_name = data.getFolderPath();
			String folder_id = data.getFolderId();
			String file_name = data.getFilePath();
			String file_id = data.getFileId();
			
			System.out.println("ID = " + id);
			System.out.println("Folder name = " + folder_name);
			System.out.println("Folder id = " + folder_id);
			System.out.println("File name = " + file_name);
			System.out.println("File id = " + file_id);
			System.out.println();
		}
	}
	
	/**
	 * Delete table
	 * @param table Selected table to delete from
	 * @param name Name to be deleted
	 * @param id Id reference of name
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void delete(String table, String name, String id) throws ClassNotFoundException, SQLException {
		Statement stmt = c.createStatement();
		String sql = "DELETE FROM " + table + "WHERE ( (folder_name = " + "\"" + name + "\"" + " AND folder_id = " + "\"" + id + "\"" +")" + " OR " + "(file_name = " + "\"" + name + "\"" + " AND " + "file_id = " + "\"" + id + "\") )";
		stmt.executeQuery(sql);
	}
	
	/**
	 * Update table
	 * @param table Selected table to be updated
	 * @param file_or_folderName File or Folder name to be updated
	 * @param file_or_folderID File or Folder id to be updated
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void update(String table, String name, String id) throws SQLException, ClassNotFoundException {
	}
	
	/**
	 * Find keyword in database and return a list of the search result
	 * @param keyword Specified keyword
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public List<SkyDriveData> find(String table, String keyword) throws SQLException, ClassNotFoundException {
		connect();
		String query = "SELECT * from " + table + " WHERE (folder_name = ? OR folder_id = ? OR file_name = ? OR file_id = ?)";
		PreparedStatement ps = this.c.prepareStatement(query);
		ps.setString(1, keyword);
		ps.setString(2, keyword);
		ps.setString(3, keyword);
		ps.setString(4, keyword);
		List<SkyDriveData> skyDriveData = new ArrayList<SkyDriveData>();
		
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			SkyDriveData skyData = new SkyDriveData();
			skyData.setId(rs.getInt("id"));
			skyData.setFolderId(rs.getString("folder_id"));
			skyData.setFolderId(rs.getString("folder_name"));
			skyData.setFileId(rs.getString("file_id"));
			skyData.setFilePath(rs.getString("file_name"));
			skyDriveData.add(skyData);
		}
		
		rs.close();
		close();
		ps.close();
		
		return skyDriveData;
	}
	
	/**
	 * Drop table
	 * @param table Table to be dropped
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public void drop(String table) throws ClassNotFoundException, SQLException {
		
		String sql = "DROP TABLE " + table;
		Statement stmt = c.createStatement();
		stmt.executeUpdate(sql);
	}
	
	public void dropDB()  {
		
		try {
			String sql = "DROP DATABASE " + this.database;
			Statement stmt = this.c.createStatement();
		
			stmt.executeUpdate(sql);
		} catch(SQLException se) {
			se.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
