package com.cloudette.tests;

import com.cloudette.connectivity.Client;

public class TestClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String request = "Hello Server!";
		String server = "10.0.0.104";
		int port = 8000;
		
		Client client = new Client(server, port);
		client.sendToSever(request);
		System.out.println("Answer: " + client.getAnswer());
	}

}
