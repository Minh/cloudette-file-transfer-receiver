package com.cloudette.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import com.cloudette.data.Data;

public class TestData {

	public static void main(String[] args) throws FileNotFoundException, IOException, NoSuchAlgorithmException {
		// TODO Auto-generated method stub
		String path1 = "C:\\Users\\Minh-Long\\Downloads\\Nim.hs";
		
		System.out.println("Starting Class Data test\n");
		
		Data data1 = new Data(path1);
		data1.setContent();
		data1.summary();

		System.out.println("End Class Data test");
	}

}
