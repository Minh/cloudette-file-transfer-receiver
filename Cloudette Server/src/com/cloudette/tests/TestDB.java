package com.cloudette.tests;

import java.sql.SQLException;

import com.cloudette.database.DBHandler;

public class TestDB {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		DBHandler db = new DBHandler("TestDB.db");
		db.connect();
		
		db.drop("TestTable");
		
		db.createTable("TestTable");
		
		db.insert("TestTable", 1, "folder_name", "folder_id", "filename", "file_id");
		
		db.printQuery("TestTable", "folder_name", "folder_id");
		
		db.drop("TestTable");
		
		db.close();
	}

}
